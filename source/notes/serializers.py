from rest_framework import serializers

from source.notes.models import Note, Group


class NoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Note
        fields = ('id', 'group', 'title', 'content', 'created_at', 'updated_at')


class GroupSerializer(serializers.ModelSerializer):
    notes = serializers.StringRelatedField(read_only=True, many=True)

    class Meta:
        model = Group
        fields = ('id', 'title', 'notes')
